#include <bits/stdc++.h>
#include <vector> 
using namespace std;

bool isPossible(int seps[], int N, int C, int mid) 
{ 
    int elem = 1;
    int pos = seps[0];  
  
    for (int i = 1; i < N; i++) { 
        if (seps[i] - pos >= mid) { 
            pos = seps[i]; 
            elem++; 
  
            if (elem == C) 
              return true; 
        } 
    } 
    return false; 
} 

int main(){
    int N;
    int C;
    int seps[100000];
    
    cin >> N;
    cin >> C;
    for (int i = 0; i < N; i++) {
        //int a;
        cin >> seps[i];
       // seps.push_back(a);
    }

    sort(seps, seps + N);
  
    int low = 0;
    int high = seps[N-1];
    int res = -1;
    int mid;
  
    while (low <= high) { 
        mid = (low + high)/2; 
   
        if (!isPossible(seps, N, C, mid)) { 
            high = mid - 1; 
        } else {
            res = max(res, mid); 
            low = mid + 1;         } 
    }

    cout << res; 

    //for (vector<int>::iterator it = seps.begin(); it != seps.end(); ++it) cout << ' ' << *it;

}