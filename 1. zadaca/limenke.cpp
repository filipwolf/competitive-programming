#include <bits/stdc++.h>
#include <vector> 
#include <algorithm>
using namespace std;

int main(){

    //cout.setf(ios::fixed);
    //cout.precision(0);
    
    unsigned int N;
    long long sum = 0;
    vector<long long> cans;
    
    cin >> N;
    for (unsigned int i = 0; i < N; i++) {
        unsigned int a;
        cin >> a;
        cans.push_back(a);
    }

    sort(cans.begin(), cans.end());
    reverse(cans.begin(), cans.end());

    for (unsigned int i = 0; i < N; i++) {
        sum += i*cans[i] + 1;
    }
    cout << sum;
    //for (vector<long long>::iterator it = cans.begin(); it != cans.end(); ++it) cout << ' ' << *it;
 
}