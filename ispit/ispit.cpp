#include <bits/stdc++.h>
#include <vector>

using namespace std;

int points[100000];
int mistakes[100000];

int find(int N, int correct, int result) {
    if(N == 0) return 0;
    int a = 0;
    if(correct != 3) {
        a = find(N-1, correct+1, result);
    }
    int b = find(N-1, 0, result);

    int c = max(a, b);
    result = max(c, result);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int N;

    cin >> N;


    for (int i = 1; i <= N; i++)
    {
        cin >> points[N-i]; 
    }
    for (int i = 1; i <= N; i++)
    {
        cin >> mistakes[N-i]; 
    }

    int result = 0;
    int correct = 0;

    result = find(N, correct, result);
}