#include <bits/stdc++.h>
#include <vector>

using namespace std;

int main() {

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    long long N, M;
    cin >> N >> M;
    long long a = 0;
    //long long diff[100];
    //long long ft[100];

    vector<long long> kobasice(N+1, 0);

    for (long long i = 1; i <= N; i++)
    {
        long long b;
        cin >> b;
        kobasice[i] = b;
        a += b;

    }
    long long cnt = 0;
    sort(kobasice.begin(), kobasice.end());
    vector<long long> difarr(10000000, 0);
    vector<long long> ft(10000000, 0);

    for (long long i = 1; i <= N; i++)
    {
        
        difarr[i] += (N-i + 1)*kobasice[i];
        // difarr[N + 1] --;
        // long long x = 1;
        // for(; x <= 10000000; x += x&-x) ft[x] ++;
        // x = N + 1;
        // for(; x <= 10000000; x += x&-x) ft[x] --;
         
    }
    long long last = 0;
    while(a > M) {
        cnt++;
        //if()
        long long k = cnt;
        long long s = 0;
        while (k >= 1) {    
            s += ft[k];
            k -= k&-k;
        }
        s += last;
        a -= s;
        s-= last;
        last = s;
    }

    if(a < M) cnt--;
    
 cout << cnt;

    
}