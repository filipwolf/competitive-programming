#include <bits/stdc++.h>
#include <vector>

using namespace std;

unordered_map< int, vector<int> > store; 

int findFrequency(vector<int> &sieve, int n, int left, 
                      int right, int element) 
{ 
    int a = lower_bound(store[element].begin(), 
                        store[element].end(), 
                        left) 
            - store[element].begin(); 
  
    int b = upper_bound(store[element].begin(), 
                        store[element].end(), 
                        right) 
            - store[element].begin(); 
  
    return b-a; 
} 

int main() {
    
    //ios_base::sync_with_stdio(false);
    //cin.tie(NULL);

    int q;
    scanf("%d", &q);

    vector<int> sieve(100000, 1);

    
    for (int x = 2; x <= 100000; x++) {
        //if (sieve[x] != 0) continue;
        for (int u = x; u <= 100000; u += x) {
            sieve[u - 1]++;
        }
    }


    for (int i=0; i<100000; ++i) 
        store[sieve[i]].push_back(i+1);

    
    for (int i = 0; i < q; i++)
    {
        int o, b, c;
        scanf("%d %d %d", &o, &b, &c);
        
        printf("%d\n", findFrequency(sieve, 100000, o, b, c));

    }    
}