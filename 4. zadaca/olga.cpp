#include <bits/stdc++.h>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;
// long long Pascal(long long n, long long b) 
// { 
//     if (b == 0 || b == n) return 1;
      
//     for (long long line = 1; line <= n + 1; line++) 
//     { 
//         long long C = 1;
//         for (long long i = 1; i <= line; i++)  
//         { 
            
//             C = (C * (line - i) / i);  
//             if (line == n + 1 && i == b) return C;
//         } 
//     } 
// }  
long long l[2000][2000] = { 0 }; 
  
long long initialize() 
{ 
  
    l[0][0] = 1; 
    for (long long i = 1; i < 2000; i++) { 
        l[i][0] = 1; 
        for (long long j = 1; j < i + 1; j++) { 
  
            l[i][j] = (l[i - 1][j - 1] + l[i - 1][j])%1000000007; 
        } 
    } 
} 
long long nCr(long long n, long long r) 
{ 
    return l[n][r]; 
} 
int main() {
        initialize();
        long long m, n, p;
        cin >> m >> n >> p;

        vector<long long> field1(p, 0);
        vector<long long> field2(p, 0);

        for (long long i = 0; i < p; i++)
        {
            cin >> field1[i] >> field2[i];
        }

        long long base = nCr(m+n, m);
        long long kul = 0b00000000000000000000;

        long long final = base;

        for (long long cnt = 0b00000000000000000001; cnt <= pow(2, p) - 1; cnt++) {
            long long count = 0;
            long long array[20];
            long long l = cnt;
            long long br = 0;
            long long prev1, prev2;
            prev1 = 0;
            prev2 = 0;
            while(l != 0) {
                if(l & 1 == 1) {
                    array[count] = br;
                    if (field1[array[count]] < prev1 || field2[array[count]] < prev2) {
                        array[count] = array[count-1];
                        array[count-1] = br;
                    }
                    prev1 = field1[array[count]];
                    prev2 = field2[array[count]];
                    count++;
                }
                l = l >> 1;
                br++;
            }
            long long res = 1;
            prev1 = 0;
            prev2 = 0;
            for (long long i = 0; i <= count; i++)
            {
                if (i == count) {
                    res *= nCr(m - prev1 + n - prev2, m - prev1);
                    res %= 1000000007;
                    break;
                }
                if(field1[array[i]] < prev1 || field2[array[i]] < prev2) {
                    res = 0;
                    break;
                }

                res *= nCr(field1[array[i]] - prev1 + field2[array[i]] - prev2, field1[array[i]] - prev1);
                res %= 1000000007;
                prev1 = field1[array[i]];
                prev2 = field2[array[i]];
            }

            if(count%2 == 0) final += res;
            else final -= res;
            final %= 1000000007;
            
            
        }
        final = final%1000000007;
        if (final < 0) {
            while (final < 0)
            {
                final += 1000000007;
            }
            
        }
        cout << final;
        
        // for (int i = 2; i <= p; i++)
        // {
        //     int res = 0;
        //     for (int j = 0; j < p; j++)
        //  
        //         int inter = 1;
        //         int curr1 = field1[j];
        //         int curr2 = field2[j];
        //         for (int k = 0; k < i; k++)
        //         {
        //             if (k == j) continue;
        //             if((field1[k] < field1[k-1] || field2[k] < field2[k-1]) && k != 0) continue;
        //             inter *= factorial(field1[k] - field1[k - 1]+field2[k] - field2[k - 1])/(factorial(field1[k])*factorial(field2[k]));
        //         }
                
        //         res += inter;
        //     }
        //     if (i%2 == 0) final -= res;
        //     else final += res;
        // }
        
        //cout << final;
        
}