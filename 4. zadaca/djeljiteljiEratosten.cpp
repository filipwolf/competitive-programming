#include <bits/stdc++.h>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int q;
    cin >> q;

    //vector<vector<int>> upiti(q, vector<int>(3, 0));
    vector<int> sieve(100000, 0);

    for (int x = 2; x <= 100000; x++) {
        if (sieve[x] != 0) continue;
        for (int u = 2*x; u <= 100000; u += x) {
            sieve[u] = x;
        }
        if (sieve[x] == 0) sieve[x] = x;
    }

    for (int i = 0; i < q; i++)
    {
        int o, b, c;
        cin >> o >> b >> c;
        int brojac = 0;
        for (int j = o; j <= b;j++) {
            int cnt = 0;
            int nekaj = 0;
            int a = sieve[j];
            if (j == 1) {
                nekaj = 1;
            }
            else if (a == j) {
                nekaj = 2;
            } else {
                vector<int> factors;
                int k = j;
                while (k > 1) {
                    factors.push_back(sieve[k]);
                    k /= sieve[k];
                }
                factors.push_back(0);
                int next = factors[1];
                int product = 1;
                for (int h = 0; h < factors.size()-1; h++)
                {
                    next = factors[h+1];
                    cnt++;
                    if (h == factors.size()-1) cnt++;
                    if (next != factors[h]) {
                        cnt++;
                        product *= cnt;
                        cnt = 0;
                    }
                }
                if (product == c) brojac++;
            }
            if (nekaj == c) brojac++;
        }
        cout << brojac << "\n";
    }
 
    // for (int i = 0; i < q; i++) {

    //     int brojac = 0;
    //     for (int j = upiti[i][0]; j <= upiti[i][1];j++) {
    //         int cnt = 0;
    //         int nekaj = 0;
    //         int a = sieve[j];
    //         if (j == 1) {
    //             nekaj = 1;
    //         }
    //         else if (a == j) {
    //             nekaj = 2;
    //         } else {
    //             vector<int> factors;
    //             int k = j;
    //             while (k > 1) {
    //                 factors.push_back(sieve[k]);
    //                 k /= sieve[k];
    //             }
    //             factors.push_back(0);
    //             int next = factors[1];
    //             int product = 1;
    //             for (int h = 0; h < factors.size()-1; h++)
    //             {
    //                 next = factors[h+1];
    //                 cnt++;
    //                 if (h == factors.size()-1) cnt++;
    //                 if (next != factors[h]) {
    //                     cnt++;
    //                     product *= cnt;
    //                     cnt = 0;
    //                 }
    //             }
    //             if (product == upiti[i][2]) brojac++;
    //         }
    //         if (nekaj == upiti[i][2]) brojac++;
    //     }
    //     cout << brojac << "\n";
    // }
}