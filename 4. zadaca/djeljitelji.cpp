#include <bits/stdc++.h>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> factors(int n) {
    vector<int> f;
    for (int x = 2; x*x <= n; x++) {
        while (n%x == 0) {
            f.push_back(x);
            n /= x;
        }
    }
    if (n > 1) f.push_back(n);
    return f;
    }

int main() {

    int q;
    cin >> q;

    vector<vector<int>> upiti(q, vector<int>(3, 0));

    for (int i = 0; i < q; i++)
    {
        cin >> upiti[i][0] >> upiti[i][1] >> upiti[i][2];
    }
    for (int i = 0; i < q; i++) {
        int brojilo = 0;
        for (int j = upiti[i][0]; j <= upiti[i][1]; j++)
        {
            vector<int> f;
            if (j != 1) {
                int n = j;
                for (int x = 2; x*x <= n; x++) {
                    while (n%x == 0) {
                        f.push_back(x);
                        n /= x;
                    }
                }
                if (n > 1) f.push_back(n);
            }
            else {
                f.push_back(1);
            }
            f.push_back(0);
            int next = f[1];
            int cnt = 0;
            int product = 1;
            for (int k = 0; k < f.size()-1; k++)
            {
                next = f[k+1];
                cnt++;
                if (k == f.size()-1) cnt++;
                if (next != f[k]) {
                    cnt++;

                    product *= cnt;
                    cnt = 0;
                }
            }
            //if (j == 1) product--;
            //if (product == 1 && j != 1) product = cnt;
            if (product == upiti[i][2]) brojilo++;
            if (f[0] == 1) brojilo ++;
        }
        cout << brojilo << "\n";
    }   
}