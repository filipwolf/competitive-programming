#include <bits/stdc++.h>
#include <vector> 
     
using namespace std;

#define LSOne(S) (S & (-S))

	

// // Array size
// int N;

// // Point query: Returns the value at position b in the array
// int query(int b)	{
// 	int sum = 0;
// 	for (; b; b -= LSOne(b)) sum += ft[b];
// 	return sum;
// }

// // Point update: Adds v to the value at position k in the array
// void update(int k, int v) {
// 	for (; k <= N; k += LSOne(k)) ft[k] += v;
// }

// // Range update: Adds v to each element in [i...j] in the array
// void range_update(int i, int j, int v)	{
// 	update(i, v);
// 	update(j + 1, -v);
// }

// int sum(int k) {
//     int s = 0;
//     while (k >= 1) {
//         s += ft[k];
//         k -= k&-k;
//     }
//     return s;
// }
     
int main() {
    
    long long Q;
    long long N;

    long long sectors[1000000], diff[1000000];
    long long ft[1000000];
    
    cin >> N;
    cin >> Q;
    
    
    //vector<vector<int>> upit(Q, vector<int>(4, 0));
    long long answers[Q];
    long long i;
    
    for(i = 1; i <= N; i++) {
        cin >> sectors[i];
        diff[i] = sectors[i] - sectors[i-1];
        long long x = i;
        for(; x <= N; x += x&-x) ft[x] += diff[i];
    }
    
    long long b = 0;
    for(long long i = 0; i < Q; i++) {
        long long n;
        cin >> n;
        if (n == 1) {
            long long one;
            long long two;
            long long three;

            cin >> one;
            cin >> two;
            cin >> three;
            diff[one] += three;
            diff[two + 1] += -three;
            long long x = one;
            for(; x <= N; x += x&-x) ft[x] += three;
            x = two + 1;
            for(; x <= N; x += x&-x) ft[x] += -three;
    
        }
        else {
            long long k;
            cin >> k;
            long long s = 0;
            while (k >= 1) {    
                s += ft[k];
                k -= k&-k;
            }
            answers[b] = s;
            b++;
        }
        
    }
    
    for(long long i = 0; i < b; i++) {
        cout << answers[i] << "\n";
    }
    
}