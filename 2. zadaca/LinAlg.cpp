#include <bits/stdc++.h>
#include <vector> 

using namespace std;

int main() {
    int A, B, C, Q;

    cin >> A;
    cin >> B;
    cin >> C;

    vector<vector<vector<int>>> matrix(A + 1, vector<vector<int>>(B +1 , vector<int>(C + 1, 0)));
    vector<vector<vector<int>>> xorArray(A + 1, vector<vector<int>>(B + 1, vector<int>(C + 1, 0)));

    for (int i = 1; i < A + 1; i++) {
        for (int j = 1; j < B + 1; j++) {
            for (int k = 1; k < C + 1; k++) {
                cin >> matrix[i][j][k];
            }
        }
    }

    cin >> Q;

    vector<vector<int>> pointA(Q, vector<int>(3, 0));
    vector<vector<int>> pointB(Q, vector<int>(3, 0));

    for (int i = 0; i < Q; i++) {
        for (int j = 0; j < 3; j++) {
            int n;
            cin >> n;
            n -= 1;
            pointA[i][j] = n;
        }
        for (int j = 0; j < 3; j++) {
            int n;
            cin >> n;
            n -= 1;
            pointB[i][j] = n;
        }
    }

    for (int i = 1; i < A + 1; i++) {
        for (int j = 1; j < B + 1; j++) {
            for (int k = 1; k < C + 1; k++) {
                if (i == 1 && j == 1 && k == 1) {
                    xorArray[i][j][k] = matrix[i][j][k]; 
                } 
                else if (i == 1 && j == 1 && k == 2) {
                    xorArray[i][j][k] = matrix[i][j][k]^matrix[i][j][k-1]; 
                }
                else if (i == 1 && j == 2 && k == 1) {
                    xorArray[i][j][k] = matrix[i][j][k]^matrix[i][j-1][k]; 
                }
                else if (i == 2 && j == 1 && k == 1) {
                    xorArray[i][j][k] = matrix[i][j][k]^matrix[i-1][j][k]; 
                }
                else if (i == 1 && j == 2 && k == 2) {
                    xorArray[i][j][k] = matrix[i][j][k]^matrix[i][j][k-1]^matrix[i][j-1][k]^matrix[i][j-1][k-1]; 
                }
                else if (i == 2 && j == 2 && k == 1) {
                    xorArray[i][j][k] = matrix[i][j][k]^matrix[i-1][j][k]^matrix[i][j-1][k]^matrix[i-1][j-1][k]; 
                }
                else if (i == 2 && j == 1 && k == 2) {
                    xorArray[i][j][k] = matrix[i][j][k]^matrix[i][j][k-1]^matrix[i-1][j][k]^matrix[i-1][j][k-1]; 
                } 
                else {
                xorArray[i][j][k] = matrix[i][j][k]^xorArray[i-1][j][k]^xorArray[i][j-1][k]^xorArray[i][j][k-1]^xorArray[i][j-1][k-1]
                ^xorArray[i-1][j][k-1]^xorArray[i-1][j-1][k]^xorArray[i-1][j-1][k-1];
                }
            }
        }
    }

    for (int i = 0; i < Q; i++) {
        printf("%d\n" , xorArray[pointB[i][0]+1][pointB[i][1]+1][pointB[i][2]+1]^xorArray[pointB[i][0]+1][pointB[i][1]+1][pointA[i][2]]
        ^xorArray[pointB[i][0]+1][pointA[i][1]][pointB[i][2]+1]^xorArray[pointA[i][0]][pointB[i][1]+1][pointB[i][2]+1]^xorArray[pointA[i][0]][pointA[i][1]][pointB[i][2]+1]
        ^xorArray[pointA[i][0]][pointB[i][1]+1][pointA[i][2]]^xorArray[pointB[i][0]+1][pointA[i][1]][pointA[i][2]]^xorArray[pointA[i][0]][pointA[i][1]][pointA[i][2]]);
    }


}