    #include <bits/stdc++.h>
    #include <vector>
    #include <algorithm>
    #define MIN 0
     
    using namespace std;
     
     
    // int solve(int N, int M, char last, vector<vector<char>> &polje, vector<vector<int>> &zbroj, vector<vector<vector<bool>>> &ready) {
    //     //cout << polje[N][M];
    //     if (N == N1 || M == M1) {
    //         return 0;
    //         // if (polje[N][M] == 'X') {
    //         //     ready[N][M] = true;
    //         //     zbroj[N][M] = 1;
    //         //     return zbroj[N][M];
    //         // }
    //         // else {
    //         //     ready[N][M] = true;
    //         //     zbroj[N][M] = 0;
    //         //     return zbroj[N][M];
    //         // }
    //     }
    //     //if (N == 0 || M == 0) return 0;
    //     int gd=0, g=0, d=0, a=0;
    //     if (last == 'd') {
    //         if (ready[N][M+1][0] == true) g = zbroj[N][M+1];
    //         else g = solve(N, M+1, 'g', polje, zbroj, ready);
    //         if (ready[N+1][M+1][2] == true) gd = zbroj[N+1][M+1];
    //         else gd = solve(N+1, M+1, 'a', polje, zbroj, ready);
    //         a = max(g, gd);
    //         zbroj[N][M] = a;
    //         if (polje[N][M] == 'X') zbroj[N][M] += 1;
     
    //     } else if (last == 'g') {
    //         if (ready[N+1][M][1] == true) d = zbroj[N+1][M];
    //         else d = solve(N+1, M, 'd', polje, zbroj, ready);
    //         if (ready[N+1][M+1][2] == true) gd = zbroj[N+1][M+1];
    //         else gd = solve(N+1, M+1, 'a', polje, zbroj, ready);
    //         a = max(d, gd);
    //         zbroj[N][M] = a;
    //         if (polje[N][M] == 'X') zbroj[N][M] += 1;
     
    //     } else if (last == 'a') {
    //         if (ready[N+1][M][1] == true) d = zbroj[N+1][M];
    //         else d = solve(N+1, M, 'd', polje, zbroj, ready);        
    //         if (ready[N][M+1][0] == true) g = zbroj[N][M+1];
    //         else g = solve(N, M+1, 'g', polje, zbroj, ready);
    //         a = max(g, d);
    //         zbroj[N][M] = a;
    //         if (polje[N][M] == 'X') zbroj[N][M] += 1;
     
    //     } else {
    //         if (ready[N+1][M][1] == true) d = zbroj[N+1][M];
    //         else d = solve(N+1, M, 'd', polje, zbroj, ready);
    //         if (ready[N][M+1][0] == true) g = zbroj[N][M+1];
    //         else g = solve(N, M+1, 'g', polje, zbroj, ready);
    //         if (ready[N+1][M+1][2] == true) gd = zbroj[N+1][M+1];
    //         else gd = solve(N+1, M+1, 'a', polje, zbroj, ready);
    //         a = max(d, g);
    //         zbroj[N][M] = max(a, gd);
    //         if (polje[N][M] == 'X') zbroj[N][M] += 1;
    //     }
    //     if (last == 'g') ready[N][M][0] = true;
    //     else if (last == 'd') ready[N][M][1] = true;
    //     else if (last == 'a') ready[N][M][2] = true;
    //     if (zbroj[N][M] > global) global = zbroj[N][M];
    //     //cout << zbroj[N][M];
    //     return zbroj[N][M];
    // }
     
    int main() {

        int N1, M1, global = -1;
     
        cin >> N1 >> M1;
     
        vector<vector<char>> polje(N1+1, vector<char>(M1+1, 0));
        //vector<vector<int>> zbroj(N1+1, vector<int>(M1+1, 0));
        vector<vector<vector<int>>> ready(N1+1, vector<vector<int>>(M1+1, vector<int>(3, 0)));
     
        for (int i=1;i<=N1;i++) {
            for (int j=1;j<=M1;j++) {
                cin >> polje[i][j];
            }
        }
        int maxn=0;
     
        //char last = 'N';

        for (int i=1;i<=N1;i++) {
            for (int j=1;j<=M1;j++) {
                if (i > 2*j || j > 2*i) continue;

                int up = 0, left = 0, upLeft = 0;

                up = max(ready[i-1][j][1], ready[i-1][j][2]);
                left = max(ready[i][j-1][0], ready[i][j-1][2]);
                upLeft = max(ready[i-1][j-1][0], ready[i-1][j-1][1]);
                if (polje[i][j] == 'X') {
                    up += 1;
                    left += 1;
                    upLeft += 1;
                }
                ready[i][j][0] = up;
                ready[i][j][1] = left;
                ready[i][j][2] = upLeft;
                int a = max(up, left);       
                int b = max(a, upLeft);
                maxn = max(b, maxn);
            }
        }
        //maxn = solve(0, 0, last, polje, zbroj, ready);

        //cout << zbroj[0][0] << "\n";   
        cout << maxn;
    }