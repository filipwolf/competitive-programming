    #include <bits/stdc++.h>
    #include <vector> 
    #include <algorithm>
    #define BIG 1000000
     
    using namespace std;
    int N, M;
     
    vector<bool> ready(M+1, 0);
    vector<int> value(M+1, 0);
     
    int solve(int x, vector<int> &kovanice, int boja, vector<int> &boje, vector<vector<int>> &colorV) {
        if (x < 0) return BIG;
        if (x == 0) return 0;
        
        if (boja != -1){
            if (colorV[x][boja] != -1) return colorV[x][boja];
        }
        
        int best = BIG;
     
        for (int i = 0; i < N; i++) {
            if (boja == boje[i]) continue;
            
            int novaBoja = boje[i];
            int iznos = kovanice[i];
     
            int bla = solve(x - iznos, kovanice, novaBoja, boje, colorV);
     
            //if (bla != BIG && bla != 0) colorV[x][novaBoja] = bla+1;
            best = min(best, bla+1);
     
            //if (best != BIG && best != 1) colorV[x][boja2] = best;
        }
        colorV[x][boja] = best;
        return best;
    }
     
    int main() {
     
        ios_base::sync_with_stdio(false);
        cin.tie(NULL);
     
        cin >> N >> M;
        int number;
     
        vector<int> kovanice(N, 0);
        vector<int> boje(N, 0);
        //vector<int> value(M + 1, 0);
        vector<vector<int>> colorV(M+1, vector<int>(6, 0));
        //vector<char> colors(5,0);
     
        /*colors[0] = 'B';
        colors[1] = 'C';
        colors[2] = 'P';
        colors[3] = 'Z';
        colors[4] = 'S';*/
        char boja;
     
        for (int i=0;i<N;i++) {
            cin >> kovanice[i] >> boja;
            if(boja == 'B') boje[i] = 0;
            if(boja == 'C') boje[i] = 1;
            if(boja == 'P') boje[i] = 2;
            if(boja == 'Z') boje[i] = 3;
            if(boja == 'S') boje[i] = 4;
        }
     
        for (int i = 0; i <= M; i++) {
            for (int j = 0;j < 5; j++) {
                colorV[i][j] = -1;
            }
        }
        int a = -1;
        //bool flag = true;
        //bool flag2 = true;
     
        number = solve(M, kovanice, a, boje, colorV);
        cout << number << "\n";
      
        /*for (int i = 0; i <= M; i++) {
            for (int j = 0; j < 5; j++) {
                cout << colorV[i][j] << " ";
            }
            cout << endl;
        }*/
    }