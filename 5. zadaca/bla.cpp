#include <bits/stdc++.h>
    
using namespace std;
    
int visited[300000];
bool ready[300000];
    
int dfs(int cnt, int x, vector<vector<int>> &adj, int depth) {
    if(ready[x]) return visited[x];
    if(adj[x].size() == 0) return 1;
    
    for (int y : adj[x]) {
        cnt = max(dfs(0, y, adj, depth) + 1, cnt);
    }
    ready[x] = true;
    visited[x] = cnt;
    return cnt;
}
    
int main() {
    
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int N, M;
    cin >> N >> M;
    
    vector<vector<int>> adj(300000, vector<int>(0, 0));
    set<int> nodes;
    
    
    for(int i = 0; i<M;i++) {
        int a, b;
        cin >> a;
        cin >> b;
        adj[a].push_back(b);
        nodes.insert(a);
    }
    int best = 0;
    
    for (set<int>::iterator it=nodes.begin(); it!=nodes.end(); ++it) {
        int cnt = 0;
        int depth = 0;
        int a;
        if(ready[*it]) a = visited[*it];
        else a = dfs(cnt, *it, adj, depth);
        best = max(a, best);
        if(best == N) break;
    }
    cout << best;
}