#include <bits/stdc++.h>
    
using namespace std;

int visited[300000];
bool ready[300000];
int done[300000];

void dfs(int x, vector<vector<int>> &adj, map<int, map<int, int>> &kul) {
    visited[x] = 1;


    for (int y : adj[x]) {
        
        if(!visited[y] )dfs(y, adj, kul);
    }
    return;
}

int main() {

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int N, M;
    
    cin >> N >> M;
    
    vector<vector<int>> adj(300000, vector<int>(0, 0));
    set<int> nodes;
    map<int, map<int, int>> kul;

    for (int j=0;j<=M;j++) {
        int a, b, c;
        cin >> a >> b >> c;
        adj[a].push_back(b);
        nodes.insert(a);
        kul[a].insert(pair<int, int>(b, c));
    }
    for (set<int>::iterator it=nodes.begin(); it!=nodes.end(); ++it) {
        int a = 0;
        int bla = 0;
        int size = 0;
        dfs(*it, adj, kul);
    }
}