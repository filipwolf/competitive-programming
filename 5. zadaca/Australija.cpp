    #include <bits/stdc++.h>
        
    using namespace std;
     
    int visited[300000];
    bool ready[300000];
        
    int dfs(int max, int x, vector<vector<int>> &adj, int depth) {
        depth++;
        
        for (int y : adj[x]) {
            if(ready[y]) {
                if(visited[y] + depth > max) max = visited[y] + depth;
            }
            else max = dfs(max, y, adj, depth);
        }
        
        if(depth > max) max = depth;
        
        return max;
    }
        
    int main() {
        
        ios_base::sync_with_stdio(false);
        cin.tie(NULL);
        int N, M;
        cin >> N >> M;
        
        vector<vector<int>> adj(300000, vector<int>(0, 0));
        set<int> nodes;
        
        
        for(int i = 0; i<M;i++) {
            int a, b;
            cin >> a;
            cin >> b;
            adj[a].push_back(b);
            nodes.insert(a);
        }
        int max = 0;
        int cnt = 0;
        
        for (set<int>::iterator it=nodes.begin(); it!=nodes.end(); ++it) {
            int depth = 0;
            max = dfs(max, *it, adj, depth);
            if(max == N) break;
            visited[*it] = max;
            ready[*it] = true;
        }
        cout << max;
    }