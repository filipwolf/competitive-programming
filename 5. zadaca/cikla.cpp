#include <bits/stdc++.h>
    
using namespace std;

int N, M;
int maxl = 0;
bool flag = false;

void dfs(pair<int, int> a, vector<vector<int>> &polje2, vector<pair<int, int>> &visited, vector<pair<int, int>> &koord, int smallest, pair<int, int> J, int bla) {
    if(polje2[a.first][a.second] < bla) return;
    if(a.first == J.first && a.second == J.second) {
        if(smallest > maxl) maxl = smallest;
        flag = true;
        return;
    }
    for(int i = 0; i < koord.size();i++){
        if(koord[i].first == a.first && koord[i].second == a.second) return;
    }
    for(int i = 0; i < visited.size();i++){
        if(visited[i].first == a.first && visited[i].second == a.second) return;
    }
    visited.push_back(a);
    pair<int, int> b;

    if(polje2[a.first][a.second] < smallest) smallest = polje2[a.first][a.second];

    for (int y = 0; y < 4; y++) {
        if(y == 0) {
            if(a.second == 0) continue;
            b = make_pair(a.first, a.second - 1);
        }
        else if(y == 1) {
            if(a.first == 0) continue;
            b = make_pair(a.first - 1, a.second);
        }
        else if(y == 2) {
            if(a.second == M - 1) continue;
            b = make_pair(a.first, a.second + 1);
        }
        else if(y == 3) {
            if(a.first == N - 1) continue;
            b = make_pair(a.first + 1, a.second);
        }
        dfs(b, polje2, visited, koord, smallest, J, bla);
        if(flag == true) return;
    }
    visited.pop_back();
    return;
}

int main() {

    
    cin >> N >> M;
    
    vector<vector<char>> polje(N, vector<char>(M, 0));
    vector<vector<int>> polje2(N, vector<int>(M, 0));
    vector<pair<int, int>> koord;
    char a = 0;
    pair<int, int> V;
    pair<int, int> J;
    for (int i=0;i<N;i++) {
        for (int j=0;j<M;j++) {
            cin >> a;
            polje[i][j] = a;
            if(a == '+') {
                koord.push_back(make_pair(i, j));
            }
            else if(a == 'V') {
                V = make_pair(i, j);
            }
            else if(a == 'J') {
                J = make_pair(i, j);
            }
        }
    }
    for (int i=0;i<N;i++) {
        for (int j=0;j<M;j++) {
            int min = 2000;
            for (int k=0;k<koord.size();k++) {
                int a = abs(koord[k].first - i) + abs(koord[k].second - j);
                if (a < min) min = a;
            }
            polje2[i][j] = min;
            //cout << min;
        }
        //cout << "\n";
    }
    vector<pair<int, int>> visited;
    int smallest = 2000;
    for (int i=N+M;i>0;i--) {
        dfs(V, polje2, visited, koord, smallest, J, i);
        if(flag == true) break;
    }
    cout << maxl;
}